import redis
import settings


class Middleware:
    def __init__(self):
        """ Initialize the connection objects"""
        self.client = redis.Redis(
                            host='localhost',
                            port=6379,
                            db=0
                        )

    def set_login(self, date, uid):
        if not self.is_present(date):
            self.client.setbit(date, (settings.USER_LIMIT - 1), 0)
        return self.client.setbit(date, uid, 1)

    def get_login(self, date):
        return self.client.getrange(date, 0, (settings.USER_LIMIT - 1))

    def get_consecutive(self, op, dest, key1, key2):
        self.client.bitop(op, dest, key1, key2)
        return self.get_login(dest)

    def get_count(self, key):
        return self.client.bitcount(key)

    def is_present(self, date):
        return self.client.exists(date)
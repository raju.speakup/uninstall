from datetime import datetime, timedelta
from flask import Flask, request

import settings
from middleware import Middleware
from utils import generate_response, get_stats_for_date
app = Flask(__name__)
redis_obj = Middleware()

"""
**************************************************************************************************************
Function Name : checkin
Purpose : This is a view function which caters to the /checkin POST endpoint.
          It registers a checkin in redis for a given user_id
**************************************************************************************************************
"""


@app.route('/api/v1/checkin', methods=['POST'])
def checkin():
    if request.method == 'POST':
        try:

            user_id = request.json['user_id']
            date = request.json['date']
            if user_id < 0 or user_id > (settings.USER_LIMIT - 1):
                return generate_response(
                            "Enter the id between 0 to {}".format(
                                str(settings.USER_LIMIT - 1)),
                                406
                            )

            try:
                date_obj = datetime.strptime(date, '%d/%m/%Y')
                yesterday = datetime.strftime(date_obj - timedelta(1), '%d/%m/%Y')

            except ValueError:
                return generate_response("Incorrect data format, should be DD/MM/YYYY "
                                         "or invalid date", 406)
        except Exception as e:
            return generate_response("The arguments user-id or date"
                                     " is not specified in the request body.",406)
        redis_obj.set_login(date, user_id)
        print(type(user_id), date, yesterday)
        return generate_response("Checked in", 200)


"""
**************************************************************************************************************
Function Name : get_stats
Purpose : This is a view function which caters to the /metrics POST endpoint.
          It retrieves all the required stats for a given date
**************************************************************************************************************
"""


@app.route('/api/v1/metrics', methods=['GET'])
def get_stats():
    if request.method == 'GET':
        try:
            date = request.args['date']

            try:
                date_obj = datetime.strptime(date, '%d/%m/%Y')
                yesterday = datetime.strftime(date_obj - timedelta(1), '%d/%m/%Y')

            except ValueError:
                return generate_response("Incorrect data format, should be DD/MM/YYYY "
                                         "or invalid date", 406)
        except Exception as e:
            return generate_response("No date specified in the arguments", 406)
        return generate_response(get_stats_for_date(date, yesterday), 200)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
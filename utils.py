import json
from flask import Response

import settings
from middleware import Middleware
redis_obj = Middleware()

"""
**************************************************************************************************************
Function Name : get_stats_for_date
Purpose : This function gets all the stats from redis for
          a given date
**************************************************************************************************************
"""


def get_stats_for_date(date, yesterday):
    stats_dict = dict()
    cons_present_list = list()
    cons_absent_list = list()
    key1 = date.encode('ascii')
    key2 = yesterday

    bit_list = list(
                    enumerate(
                        string_to_bitmap(
                            redis_obj.get_login(date)
                        )
                    )
                )
    if redis_obj.is_present(key1) and redis_obj.is_present(key2):
        cons_present_list = list(
            enumerate(
                string_to_bitmap(
                    redis_obj.get_consecutive(
                        'AND',
                        '{}:present'.format(date),
                        key1,
                        key2
                    )
                )
            )
        )
        cons_absent_list = list(
            enumerate(
                string_to_bitmap(
                    redis_obj.get_consecutive(
                        'OR',
                        '{}:absent'.format(date),
                        key1,
                        key2
                    )
                )
            )
        )
    stats_dict['count_present'] = redis_obj.get_count(date)
    stats_dict['count_absent'] = settings.USER_LIMIT - redis_obj.get_count(date)
    stats_dict['ids_present'] = [idx for idx, bit in bit_list if bit == '1' and idx < settings.USER_LIMIT]
    stats_dict['ids_absent'] = [idx for idx, bit in bit_list if bit == '0' and idx < settings.USER_LIMIT]
    stats_dict['consecutive_present'] = [idx for idx, bit in cons_present_list if bit == '1' and idx < settings.USER_LIMIT]
    stats_dict['consecutive_absent'] = [idx for idx, bit in cons_absent_list if bit == '0' and idx < settings.USER_LIMIT]
    return stats_dict


"""
**************************************************************************************************************
Function Name : string_to_bitmap
Purpose : This function converts the  string to bitmap 
**************************************************************************************************************
"""


def string_to_bitmap(string):
    bitmap = str()
    for char in string:
        x = ord(char)
        val = bin(x).split('b')[1]
        if len(val) < 8:
            val = '0' * (8 - len(val)) + val
        bitmap += val
    return bitmap


"""
**************************************************************************************************************
Function Name : generate_response
Purpose : This function generates the api response for the endpoints
**************************************************************************************************************
"""


def generate_response(content, status):
    final_dict = dict()
    final_dict['content']=content
    return Response(
                json.dumps(
                        final_dict,
                        sort_keys=True,
                        indent=4
                ),
                status=status,
                mimetype='text/html'
            )
